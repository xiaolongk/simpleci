#!/bin/bash
curl -Lo /usr/local/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/v1.16.4/bin/linux/amd64/kubectl
chmod +x /usr/local/bin/kubectl

echo "host IP"
hostIP=/sbin/ip route|awk '/default/ { print $3 }'
echo $hostIP

echo "ls current directory"
pwd
ls -la ../

echo "list of the created containers"
docker ps
docker inspect -f '{{.Config.Hostname}} - {{.Name}} - {{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(docker ps -aq)
docker inspect -f '{{.Config.Hostname}} - {{.Name}} - {{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(docker ps -aq) | grep 'control-plane' | awk -F" - " '{print $3}'

echo "list of the conf files"
docker exec newyear-control-plane whoami
docker exec newyear-control-plane ls -la /root/.kube
docker exec newyear-control-plane ls -la /etc/kubernetes/
docker exec newyear-control-plane ls -la ~/.kube
echo "mkdir -p ~/.kube"
docker exec newyear-control-plane mkdir -p ~/.kube
echo "cp /etc/kubernetes/admin.conf ~/.kube/config"
docker exec newyear-control-plane cp /etc/kubernetes/admin.conf ~/.kube/config
#docker exec newyear-control-plane cat /etc/kubernetes/admin.conf

echo "kubectl cluster-info"
docker exec newyear-control-plane kubectl cluster-info

masterIP=$(docker inspect -f '{{.Config.Hostname}} - {{.Name}} - {{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(docker ps -aq) | grep 'control-plane' | awk -F" - " '{print $3}')
echo "ifconfig"
ifconfig
echo "masterIP = $masterIP"
#cannot reach the masterIP
#ping -c 4 $masterIP
echo "KUBECONFIG = $KUBECONFIG"
masterPort=$(kubectl config view | grep 'server:' | awk -F":" '{print $4}')
echo "masterPort = $masterPort"
#kubectl cluster-info -s $masterIP:$masterPort
#kubectl cluster-info -s $hostIP:$masterPort
#kubectl cluster-info dump 

echo "========================================================================"
echo "Installing Kubernetes Dashboard"
echo "========================================================================"
echo "..."

# retrieve the yaml file with the proper version 
#kubectl --insecure-skip-tls-verify apply -f conf/dashboard-v200b8-recommended.yaml
apk add curl
echo "ls current directory in the master node"
docker exec newyear-control-plane pwd
docker exec newyear-control-plane curl -Lo ./dashboard-v200b8-recommended.yaml https://gitlab.com/xiaolongk/simpleci/raw/master/conf/dashboard-v200b8-recommended.yaml
docker exec newyear-control-plane kubectl --insecure-skip-tls-verify apply -f ./dashboard-v200b8-recommended.yaml
docker exec newyear-control-plane curl -Lo ./dashboard-adminuser.yaml https://gitlab.com/xiaolongk/simpleci/raw/master/conf/dashboard-adminuser.yaml
docker exec newyear-control-plane kubectl --insecure-skip-tls-verify apply -f ./dashboard-adminuser.yaml

echo "done"
echo "..."
echo " "

echo "========================================================================"
echo "Create sample user with the right to access the dashboard"
echo "========================================================================"
echo "..."
#kubectl --insecure-skip-tls-verify apply -f conf/dashboard-adminuser.yaml

echo "done"
echo "..."
echo " "

# Wait 5 seconds to give time for the dashboard to be deployed and the user to 
# be created
echo "..... wait 5 seconds ....."

sleep 5

echo "done"
echo "..."
echo " "

# Grep the secret and use it to login on the browser
echo "========================================================================"
echo "Get Token"
echo "========================================================================"
echo "..."

#admin_profile=$(kubectl -n kubernetes-dashboard get secret | grep admin-user | awk '{print $1}')
#dashboard_token_full=$(kubectl -n kubernetes-dashboard describe secret $admin_profile | grep "token: ")
#dashboard_token=${dashboard_token_full#"token: "}

echo "Here is the token needed to log into the dashboard:"
#docker exec newyear-control-plane kubectl -n kubernetes-dashboard get secret | grep "admin-user" | awk '{print $1}' | grep "token: "
#docker exec newyear-control-plane kubectl -n kubernetes-dashboard get secret | grep "admin-user"

docker exec newyear-control-plane kubectl -n kubernetes-dashboard describe secret $(kubectl -n kubernetes-dashboard get secret | grep admin-user | awk '{print $1}') | grep "token: "

#touch "${dashboard_token_path}"
#echo $dashboard_token > "${dashboard_token_path}"


echo "done"
echo "..."
echo " "

echo "========================================================================"
echo "Start kube proxy in another tab of the existing terminal"
echo "========================================================================"
echo "..."

#gnome-terminal --tab -- kubectl proxy -p 8001
#docker exec newyear-control-plane kubectl proxy
echo "done"
echo "..."
echo " "

echo "========================================================================"
echo "Launch dashboard in a web browser"
echo "========================================================================"
echo "..."

#xdg-open http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/
#curl http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/

#cat "${dashboard_token_path}"

echo "done"
echo "..."
echo " "

echo "========================================================================"
echo "The END"
echo "========================================================================"
